<?php 
    // var_dump($_FILES["file"]);
    if(isset($_FILES["file"])){
        $error = $_FILES["file"]["error"];
        $file = $_FILES["file"]["tmp_name"];
        $rand= rand();
        $name = "Integracion_Archivos_RvsHH.xml";
        $destination = "../data/$name";

        $upload = move_uploaded_file($file,$destination);

        if($upload){
            $res = array(
                "err"=>false,
                "status" => http_response_code(200),
                "statusText" => "Archivo $name subido con exito",
                "files" => $_FILES['file'],
                "name"=> $name
            );
        }else{
            $res = array(
                "err"=>true,
                "status" => http_response_code(400),
                "statusText" => "Error al subir acrhivo $name ",
                "files" => $_FILES['file']
            );
        }
        echo json_encode($res);
    }
?>