const d = document,
  ls = localStorage;
$html = d.querySelector("html");
$file = d.querySelector("#transmitions"),
leasings = ['postobon','central-cervecera','nutrium'];

let DATA_TABLES = {},
  noTransmition = [];

d.addEventListener("DOMContentLoaded", () => {
  // d.body.style.zoom = "80%";
  init();

  //Delegacion de eventos

  d.addEventListener("change", (event) => {
    if (event.target === $file) {
      console.log(event.target.files);
      uploader(event.target.files[0]);
    }
  });

  d.addEventListener("click", (event) => {

    if(event.target.matches("#renew")){
      toChangeTransmitionsStateDOM();
    }

    if (event.target.matches(".bgd-dont-transmited")) {
      event.target.classList.remove("bgd-dont-transmited");
      noTransmition = noTransmition.filter(
        (idTrasmition) => idTrasmition != event.target.getAttribute("data-id")
      );
      ls.setItem("no-transmition", JSON.stringify(noTransmition));
    }else if (event.target.matches(".rv")) {
      event.target.classList.add("bgd-dont-transmited");
      console.log(event.target.getAttribute("data-id"));
      noTransmition.push(event.target.getAttribute("data-id"));
      ls.setItem("no-transmition", JSON.stringify(noTransmition));
    } 
  });

  $html.addEventListener("dragover", (event) => {
    event.preventDefault();
    event.stopPropagation();
    $html.classList.add("drag");
  });

  $html.addEventListener("dragleave", (event) => {
    event.preventDefault();
    event.stopPropagation();
    $html.classList.remove("drag");
  });

  $html.addEventListener("drop", (event) => {
    event.preventDefault();
    event.stopPropagation();
    console.log(event);
    $html.classList.remove("drag");
    uploader(event.dataTransfer.files);
  });
});


async function uploader(file) {
  const formData = new FormData(),
    $divError = document.getElementById("error-message");
  formData.append("file", file[0]);
  const resUploadFile = await fetch("./php/uploader.php", {
      method: "POST",
      body: formData,
      headers: {
        "enc-type": "multipart/form-data",
        "Access-Control-Allow-Origin": "*",
      },
    }),
    jsonResUploadFile = await resUploadFile.json();

  console.log(jsonResUploadFile);
  xmlToJson(await getXMLTransmitions('Integracion_Archivos_RvsHH.xml'));
  $divError.classList.add("d-none");
}


 function toChangeTransmitionsStateDOM(){
     const $buttonRenew = d.querySelector('#renew'), 
     $transmitedState = d.querySelectorAll('.transmited'),
     $dontTransmitedState = d.querySelectorAll('.bgd-dont-transmited');

     $buttonRenew.classList.add('rotate');
     ls.setItem("no-transmition",JSON.stringify([]));

    setTimeout(()=>{
      $buttonRenew.classList.remove('rotate');
    },2000)

     $transmitedState.forEach((state)=>{
      state.classList.remove('transmited');
     });
     $dontTransmitedState.forEach((state)=>{
      state.classList.remove("bgd-dont-transmited");
     })
 }


function toDrawTables() {

  for (let i = 0; i < leasings.length; i++) {
    
    const $tamplateTable = d.getElementById(`template-table-${leasings[i]}`).content,
    $trasmitions = d.querySelector(`.content-tables-${leasings[i]}`),
    $fragment = d.createDocumentFragment();
    $trasmitions.textContent = "";

    for (const [tableN, table] of Object.entries(DATA_TABLES[leasings[i]])) {
      let codRv = "",
      tableName = `
      <div class="col-7 table-title">
      ${tableN}
      </div>`
   ;
      for (const [key, value] of Object.entries(table)) {
        if (key != 'sup') {
          codRv += `
          <div data-id="${key}"class="rv ${
            value ? "transmited" : ""
          } col-7 bgd-${leasings[i]} fw-bold">
          ${key}
          </div>`;
        } else {
          if (key == "sup") {
             $tamplateTable.querySelector(".supervisor").textContent = value;
           } else {
             $tamplateTable.querySelector(".supervisor").textContent = "";
           }
        }
      }

      $tamplateTable.querySelector("#rv-table").innerHTML = tableName + codRv;
      let $clone = d.importNode($tamplateTable, true);
      $fragment.appendChild($clone);
    }

    $trasmitions.appendChild($fragment);
  }


}


function getXMLTransmitions(xmlName) {
  return new Promise(function (resolve, reject) {
    const messageError = `Ocurrio un error al solicitar los datos de las 'Transmisiones'`;
    const xhr = new XMLHttpRequest();
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(this);
      } else if (this.status >= 400) {
        toShowMessageError(messageError + `, status:${this.status}`);
        throw new Error(messageError);
      } else {
        reject({
          status: this.status,
          staTusText: this.staTusText,
        });
      }
    };
    xhr.open("GET",  `./data/Integracion_Archivos_RvsHH.xml`, true);
    xhr.send();
  });
}


function xmlToJson(xml) {
  let allBox = 0;
  const docXML = xml.responseXML,
  $allBox = d.getElementById("all-box"),
  represents = docXML.querySelectorAll("Representante"),
  boxAll = docXML.querySelectorAll("Motivo");

  boxAll.forEach((element) => {
    allBox += parseInt(element.getAttribute("CantidadCajas"));
  });
  
  for (let i = 0; i < leasings.length; i++) {
  for (const [tableN, table] of Object.entries(DATA_TABLES[leasings[i]])) {
    for (let [rv] of Object.entries(table)) {
      represents.forEach((represent) => {
        if (rv == represent.getAttribute("Representante")) {
          DATA_TABLES[leasings[i]][tableN][rv] = true;
          d.querySelector(`div[data-id="${rv}"]`).classList.add("transmited");
          console.log(d.querySelector(`div[data-id="${rv}"]`));
        }
      });
    }
  }
}
  $allBox.textContent = `Total cajas: ${allBox}`;
}


function setNoTransmisions(lsTransmision) {
  if (lsTransmision && lsTransmision.length != 0) {
    noTransmition = JSON.parse(lsTransmision);

    noTransmition.forEach((idTransmition) => {
      const $trasmition = d.querySelector(`div[data-id="${idTransmition}"]`);
      $trasmition.classList.add("bgd-dont-transmited");
    });
  }
}

function toShowMessageError(messageError) {
  const $divError = document.getElementById("error-message"),
    $messageError = document.createElement("p");

  $messageError.textContent = `${messageError}`;
  $divError.appendChild($messageError);
  $divError.classList.remove("d-none");
}

async function getDataTables() {
  const messageError = `Ocurrio un error al solicitar los datos de las 'Mesas'`;
  try {
    let resDataTables = await fetch("./data/tables.json"),
      jsonDataTables = await resDataTables.json();
    if (!resDataTables.ok) {
      toShowMessageError(messageError + `, status:${resDataTables.status}`);
      throw new Error(messageError);
    }
    return jsonDataTables;
  } catch (err) {
    toShowMessageError(messageError);
    throw new Error(messageError);
  }
}


async function init() {
  DATA_TABLES = await getDataTables();
  toDrawTables();
  await setNoTransmisions(ls.getItem("no-transmition"));
  xmlToJson(await getXMLTransmitions('Integracion_Archivos_RvsHH.xml'));
}
